import React from 'react';
import { useLocation } from 'react-router-dom';
import './CarDetail.css'
const CarDetails: React.FC = () => {
  const location = useLocation();
  const car = location.state;

  if (!car) {
    return <div>Carro não encontrado.</div>;
  }

  return (
    <div>
      <h1>Detalhes do Carro</h1>
      <div className="car-details">
        <div className="car-image">
          <img src={car.image_url} alt={car.nome_modelo} />
        </div>
        <div className="car-info">
          <h2>{car.nome_modelo}</h2>
          <p>Ano: {car.ano}</p>
          <p>Combustível: {car.combustivel}</p>
          <p>Num. Portas: {car.num_portas}</p>
          <p>Cor: {car.cor}</p>
          <p>Valor: R$ {car.valor}</p>
        </div>
      </div>
    </div>
  );
};

export default CarDetails;
