import React, { useState } from 'react';
import { CarData } from '../types/Car';
import './List.css'
import { Link } from 'react-router-dom';
import CarForm from './CarForm';
interface CarListProps {
  cars: CarData[];
}

const CarList: React.FC<CarListProps> = ({ cars }) => {
    const [showCarForm, setShowCarForm] = useState(false);
    const [carList, setCarList] = useState(cars);
    const addCar = (newCar: CarData) => {
        setCarList([...carList, newCar]);
        setShowCarForm(false);
    };
    return (
        <>
        {showCarForm ? (
            <CarForm onAddCar={addCar} cars={cars} setShowCarForm={setShowCarForm}/>
        ) : (
            <button onClick={() => setShowCarForm(true)}>Adicionar Carro</button>
        )}
        <div className="car-list">
        {carList.map((car) => (
            <div className="car-card" key={car.id}>
                <Link to={`/car/${car.id}`} state={car}>
                    <img src={car.image_url} alt={car.nome_modelo} />
                    <div className="car-info">
                        <h2>{car.nome_modelo}</h2>
                        <p>Ano: {car.ano}</p>
                        <p>Combustível: {car.combustivel}</p>
                    </div>
                    <div className="car-price">
                        <p>Valor: R$ {car.valor}</p>
                    </div>
                </Link>
            </div>
        ))}
        </div>
        </>
    );
};

export default CarList;
