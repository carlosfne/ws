import React from 'react';
import { CarData } from '../types/Car';
import './Home.css'
import CarList from '../components/List';
const jsonData = {
    cars: [
      {
        id: 1,
        timestamp_cadastro: 1696549488,
        modelo_id: 88,
        ano: 2014,
        combustivel: 'FLEX',
        num_portas: 4,
        cor: 'BRANCA',
        nome_modelo: 'ETIOS',
        valor: 36000,
        brand: 1,
        image_url: 'https://quatrorodas.abril.com.br/wp-content/uploads/2016/11/5658b20d52657372a10739db170912-etios2.jpeg?quality=70&strip=all'
      },
      {
        id: 2,
        timestamp_cadastro: 1696531236,
        modelo_id: 77,
        ano: 2014,
        combustivel: 'FLEX',
        num_portas: 4,
        cor: 'PRETO',
        nome_modelo: 'COROLLA',
        valor: 120000,
        brand: 1,
        image_url: 'https://1.bp.blogspot.com/-neTh27CGY2k/UbIBDPsETrI/AAAAAAABCgs/0yBLWyjvIRA/s1600/Novo-Corolla-2014-Europa+(15).jpg'
      },
      {
        id: 3,
        timestamp_cadastro: 16965354321,
        modelo_id: 79,
        ano: 1993,
        combustivel: 'DIESEL',
        num_portas: 4,
        cor: 'AZUL',
        nome_modelo: 'HILUX SW4',
        valor: 47500,
        brand: 1,
        image_url: 'https://img.revendapro.com.br/vehicles/v9172837/20e7901c89ec43f926e730da3dee40c7_thumb.jpeg'
      },
      {
        id: 4,
        timestamp_cadastro: 16965398765,
        modelo_id: 101,
        ano: 2022,
        combustivel: 'FLEX',
        num_portas: 4,
        cor: 'PRATA',
        nome_modelo: 'CAMRY',
        valor: 150000,
        brand: 1,
        image_url: 'https://quatrorodas.abril.com.br/wp-content/uploads/2022/05/Camry-2-e1652889809697.jpg?quality=70&strip=info'
      },
      {
        id: 5,
        timestamp_cadastro: 1696554321,
        modelo_id: 45,
        ano: 2020,
        combustivel: 'FLEX',
        num_portas: 4,
        cor: 'VERMELHO',
        nome_modelo: 'RAV4',
        valor: 90000,
        brand: 1,
        image_url: 'https://1.bp.blogspot.com/-mIxOdI3NxV4/XOb7Dn3c68I/AAAAAAAAUt0/NaINTorfyM8Q8W5VhEMyqgc9trfldOtPQCLcBGAs/s1600/Toyota-RAV4-2020%2B%25283%2529.jpg'
      },
      {
        id: 6,
        timestamp_cadastro: 1696523456,
        modelo_id: 32,
        ano: 2019,
        combustivel: 'FLEX',
        num_portas: 4,
        cor: 'AZUL',
        nome_modelo: 'SIENNA',
        valor: 70000,
        brand: 2,
        image_url: 'https://cdn.motor1.com/images/mgl/e1VkM/s1/fiat-grand-siena-10-fire-2017.jpg'
      },
      {
        id: 7,
        timestamp_cadastro: 1696545678,
        modelo_id: 53,
        ano: 2018,
        combustivel: 'FLEX',
        num_portas: 4,
        cor: 'PRATA',
        nome_modelo: 'TUNDRA',
        valor: 85000,
        brand: 2,
        image_url: 'https://lh3.googleusercontent.com/X0eXxUDDHBVm7lEL0FGY53wqHA26Gzh8qtUvlRBT4UgDbLzjWolC64NnPzpJatuKDqasguXds99hU2euMa2HJhb8S-wotLomx55lcihEL_ti8PBNKwqp_qBhxmPrQaCBx8TLUsbs'
      }
    ]
};
  
const cars: CarData[] = jsonData.cars;

const Home: React.FC = () => {
    return (
        <div>
            <header className="page-header">
                <h3>Lista de Veículos</h3>
            </header>
            <CarList cars={cars} />
        </div>
      );
  };
  
  export default Home;
  
