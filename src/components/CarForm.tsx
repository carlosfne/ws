import React from 'react';
import toast, { Toaster } from 'react-hot-toast';
import { useForm } from 'react-hook-form';
import './CarForm.css';
import { CarData } from '../types/Car';

type CarFormProps = {
    onAddCar: (newCar: any, cars: CarData[]) => void;
    cars: CarData[];
    setShowCarForm: (show: boolean) => void;
};

const CarForm: React.FC<CarFormProps> = ({ onAddCar, cars, setShowCarForm }) => {
  const { register, handleSubmit, reset } = useForm();
  const onSubmit = (data:any) => {
    onAddCar(data, cars);
    toast('Carro adicionado com sucesso!.', {
        duration: 4000,
        position: 'top-center',
        className: '',
        icon: '👏',
        iconTheme: {
          primary: '#000',
          secondary: '#fff',
        },
    });
    reset();
    setShowCarForm(false);
  };
  const onCancel = () => {
    setShowCarForm(false);
  };

  return (
    <form className='form-container' onSubmit={handleSubmit(onSubmit)}>
      <div className="form-row">
        <div className="form-group">
          <label>Nome do Modelo:</label>
          <input {...register('nome_modelo')} required />
        </div>
        <div className="form-group">
          <label>Ano:</label>
          <input {...register('ano')} type="number" required />
        </div>
        <div className="form-group">
          <label>Combustível:</label>
          <input {...register('combustivel')} required />
        </div>
        <div className="form-group">
          <label>Num. Portas:</label>
          <input {...register('num_portas')} type="number" required />
        </div>
      </div>
      <div className="form-row">
        <div className="form-group">
            <label>Cor:</label>
            <input {...register('cor')} required />
        </div>
        <div className="form-group">
            <label>Valor:</label>
            <input {...register('valor')} type="number" step="0.01" required />
        </div>
        <div className="form-group">
            <label>Imagem URL:</label>
            <input {...register('image_url')} required />
        </div>
      </div>
      <button type="submit">Adicionar Carro</button>
      <button className='cancelar' type="button" onClick={onCancel}>Cancelar</button>
      <Toaster />
    </form>
  );
};

export default CarForm;
