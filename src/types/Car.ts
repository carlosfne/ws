export interface CarData {
    id: number;
    timestamp_cadastro: number;
    modelo_id: number;
    ano: number;
    combustivel: string;
    num_portas: number;
    cor: string;
    nome_modelo: string;
    valor: number;
    brand: number;
    image_url: string;
}
  
export interface BrandData {
    id: number;
    nome: string;
}
  
export interface GroupedCars {
    [brandId: string]: CarData[];
}